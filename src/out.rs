//! # Out
//!
//! A crate
//!
//! ```
//! use seq2kmermat::out::Out;
//! use seq2kmermat::out::OutType;
//!
//! // create an output to Stderr
//! let out = Out::new::<String>(OutType::Stderr);
//! let mut out = out.create(None).unwrap();
//!
//! // create an output to a file
//! let flog: String = "out.log".into();
//! let log = Out::new(OutType::Path(flog));
//! let mut log = log.create(None).unwrap();
//!
//! writeln!(log.out, "# start log");
//!
//! ```
//!
use std::{
    fmt,
    fs::File,
    io::{self, BufWriter, Write},
    path::PathBuf,
};

const OUT_DEFAULT_BUFSIZE: usize = 1024 * 8;

/// Enum decribing an output: Stdout, Stderr or file
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum OutType<T> {
    Path(T),
    Stdout,
    Stderr,
}

impl<T: std::fmt::Debug> fmt::Display for OutType<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            OutType::Path(path) => write!(f, "{:?}", path),
            OutType::Stdout => write!(f, "stdout"),
            OutType::Stderr => write!(f, "stderr"),
        }
    }
}

/// Struct managing an buffered output
/// can be either Stdout or Stderr or an opened file
pub struct Out {
    /// write to the provided file, or `stdout` when not provided
    pub out_path: OutType<PathBuf>,
    pub out: Box<dyn Write>,
}

impl Out {
    /// Create an Out struct
    /// Out.out is not open, it'just initialized to io::stdout()
    /// Out.create() must be used to open to the real value (stdout, stderr or file)
    pub fn new<P>(file_out: OutType<P>) -> Out
    where
        PathBuf: From<P>,
    {
        let out_path: OutType<PathBuf> = match file_out {
            OutType::Path(path) => OutType::Path(PathBuf::from(path)),
            OutType::Stdout => OutType::Stdout,
            OutType::Stderr => OutType::Stderr,
        };
        // out start with stdout. out must be with created with method create_output()
        Out {
            out_path,
            out: Box::new(io::stdout()),
        }
    }

    /// Create an Out
    /// can be either Stdout or a file depending on the value of the file_out Option
    pub fn create_file_or_stdout<P>(
        file_out: Option<P>,
        capacity: Option<usize>,
    ) -> std::io::Result<Out>
    where
        PathBuf: From<P>,
    {
        let out = match file_out {
            Some(file_out) => Self::new(OutType::Path(file_out)),
            None => Out::new(OutType::Stdout),
        };
        out.create(capacity)
    }

    /// Create an Out
    /// can be either Stderr depending on the value of the file_out Option
    pub fn create_file_or_stderr<P>(
        file_out: Option<P>,
        capacity: Option<usize>,
    ) -> std::io::Result<Out>
    where
        PathBuf: From<P>,
    {
        let out = match file_out {
            Some(file_out) => Self::new(OutType::Path(file_out)),
            None => Out::new(OutType::Stderr),
        };
        out.create(capacity)
    }

    /// Create an Out
    ///
    pub fn create_file<P>(file_out: P, capacity: Option<usize>) -> std::io::Result<Out>
    where
        PathBuf: From<P>,
    {
        let out = Self::new(OutType::Path(file_out));
        out.create(capacity)
    }
    /// create: open Out.out with buffering accordingly to the value of Out.out_path
    /// A capacity can be specified.
    /// If the value is None, a defaut capacity is configured
    pub fn create(mut self, capacity: Option<usize>) -> std::io::Result<Out> {
        //Some(ref path) => File::open(path).map(|f| Box::new(f) as Box<dyn Write>),
        let capacity = match capacity {
            Some(capacity) => capacity,
            None => OUT_DEFAULT_BUFSIZE,
        };
        match &self.out_path {
            //if let OutType::Path(path) = &self.out_path {
            OutType::Path(path) => {
                let new_out = File::create(path); //as Box<dyn Write>;
                match new_out {
                    Ok(out) => {
                        self.out =
                            Box::new(BufWriter::with_capacity(capacity, out)) as Box<dyn Write>
                    }
                    Err(e) => {
                        //drop(new_out);
                        return Err(e);
                    }
                };
            }
            OutType::Stdout => {
                self.out = Box::new(BufWriter::with_capacity(capacity, io::stdout()))
            }
            OutType::Stderr => {
                self.out = Box::new(BufWriter::with_capacity(capacity, io::stderr()))
            }
        }
        Ok(self)
    }
}

impl fmt::Debug for Out {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Out")
            .field("out_path", &self.out_path)
            .finish()
    }
}

impl fmt::Display for Out {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Out")
            .field("out_path", &self.out_path)
            .finish()
    }
}
